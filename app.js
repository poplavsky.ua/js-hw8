/*
Теоретичні питання:
1. Опишіть своїми словами що таке Document Object Model (DOM)
  Це об’єктна модель документа на основі розмітки HTML. Пунктом входу в DOM є об’єкт document, який представляє весь документ.
2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
  innerHTML повертає вміст елемента, який зберігається в ньому у вигляді строки (HTML-теги в тому числі), а innerText повертає тільки текстовий вміст елемента між відкриваючим і закриваючим HTML елементом.
3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
  Найкращі методи – це через querySelector або через querySelectorAll. Вони дають змогу виконувати пошук HTML елементів по CSS селектору.
  Також є більш застарілі методи такі як: getElementById, getElementsByClassName, getElementsByTagName, getElementsByName.
  Щодо способу, то краще звертатися напряму до елемента, наприклад через querySelector, в той час як querySelectorAll створює колекцію HTML елементів.
*/

/*
Код для завдань лежить в папці project.

1. Знайти всі параграфи на сторінці та встановити колір фону #ff0000

2. Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

3. Встановіть в якості контента елемента з класом testParagraph наступний параграф - This is a paragraph

4. Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.

5. Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
*/

//завдання 1
const paragraphBackground = document.querySelectorAll("p");
paragraphBackground.forEach(el => {el.style.background = "#ff0000"});

//завдання 2
const elOptionsList = document.querySelector("#optionsList");
console.log(elOptionsList);
console.log(elOptionsList.parentElement);
for (let node of elOptionsList.childNodes) {
  console.log(node.nodeName, node.nodeType);
};

//завдання 3
const elTestParagraph = document.querySelector("#testParagraph");
elTestParagraph.innerHTML = "This is paragraph";
console.log(elTestParagraph);

//завдання 4
const elMainHeader = document.querySelector(".main-header");
console.log(elMainHeader);
const elMainHeaderChildren = elMainHeader.children;
console.log(elMainHeaderChildren);
for (let el of elMainHeaderChildren) {
  el.classList.add("nav-item");
}
console.log(elMainHeaderChildren);

//завдання 5
const elSectionTitle = document.querySelectorAll('.section-title');
console.log(elSectionTitle);
elSectionTitle.forEach(el => {el.classList.remove("section-title")});
console.log(elSectionTitle);
